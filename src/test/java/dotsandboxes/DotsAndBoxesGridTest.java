package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    @Test
    public void testBoxComplete() {
        logger.info("Test to show that game identifies when box is complete");
        Random rand= new Random();
        int width = rand.nextInt(100)+3;
        int height = rand.nextInt(100)+3;
        DotsAndBoxesGrid testGrid = new DotsAndBoxesGrid(width, height, 2);
        int x = rand.nextInt(testGrid.width-1);
        int y = rand.nextInt(testGrid.height-1);
        int a = rand.nextInt(testGrid.width-1);
        int b = rand.nextInt(testGrid.height-1);
        int c = rand.nextInt(testGrid.width-1);
        int d = rand.nextInt(testGrid.height-1);
        // A complete square which should assert to True
        testGrid.drawHorizontal(x, y, 1);
        testGrid.drawHorizontal(x, y+1, 1);
        testGrid.drawVertical(x, y, 1);
        testGrid.drawVertical(x+1, y, 2);
        // An incomplete square which should assert to False
        testGrid.drawHorizontal(a, b, 2);
        testGrid.drawVertical(a+1, b, 1);
        testGrid.drawVertical(a, b, 1);
        assertTrue(testGrid.boxComplete(x, y));
        assertFalse(testGrid.boxComplete(a, b));
        // An unattempted square which should assert to False
        assertFalse(testGrid.boxComplete(c, d));
    }

    @Test
    public void testLineAlreadyDrawn() {
        logger.info("Test to make sure exception is thrown when playing tries to draw an line that has already been drawn");
        DotsAndBoxesGrid testGrid = new DotsAndBoxesGrid(15, 8, 2);
        Random rand= new Random();
        int x = rand.nextInt(testGrid.width-1);
        int y = rand.nextInt(testGrid.height-1);
        int a = rand.nextInt(testGrid.width-1);
        int b = rand.nextInt(testGrid.height-1);
        testGrid.drawHorizontal(x, y, 1);
        testGrid.drawVertical(a, b, 2);
        assertThrows(IllegalStateException.class, () -> testGrid.drawHorizontal(x,y, 1), "There " +
                "is already a line drawn here.");
        assertThrows(IllegalStateException.class, () -> testGrid.drawHorizontal(x,y, 2), "There " +
                "is already a line drawn here.");
        assertThrows(IllegalStateException.class, () -> testGrid.drawVertical(a,b, 1), "There " +
                "is already a line drawn here.");
        assertThrows(IllegalStateException.class, () -> testGrid.drawVertical(a,b, 2), "There " +
                "is already a line drawn here.");

    }
}
